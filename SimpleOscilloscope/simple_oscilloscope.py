###############################################
# filename : simple_oscilloscope.py
# function : 簡易オシロスコーププログラム
###############################################

from m5stack import *
from m5stack_ui import *
from machine import *
import time
# from volt_measure_board import VoltMeasureBoard     # クラス記述をファイル分割する場合はコメントアウトを外す
# from graph import Graph                             # クラス記述をファイル分割する場合はコメントアウトを外す

def tim_volt_meas_callback(timer):
    global volt_meas, graph_volt
    val_volt = volt_meas.get_volt_val(False)
    graph_volt.set_new_data(val_volt)

def buttonA_wasPressed():
    global volt_max_mode, volt_max_mode_cnt
    speaker.playTone(523, 0.2)
    volt_max_mode += 1
    if volt_max_mode >= volt_max_mode_cnt:
        volt_max_mode = 0

def buttonB_wasPressed():
    global meas_interval_mode, meas_interval_ms, MEAS_INTERVAL_MODE_CNT
    speaker.playTone(523, 0.2)
    meas_interval_mode += 1
    if meas_interval_mode >= MEAS_INTERVAL_MODE_CNT:
        meas_interval_mode = 0
    if meas_interval_mode == 0:
        meas_interval_ms = 1
    elif meas_interval_mode == 1:
        meas_interval_ms = 10
    elif meas_interval_mode == 2:
        meas_interval_ms = 100

def buttonC_wasPressed():
    global is_measure
    speaker.playTone(523, 0.2)
    is_measure = True

GPIO_ADC_PIN = 36
GPIO_RNG1_PIN = 32
GPIO_RNG2_PIN = 33
DISP_HIST_CNT = 6
TIM_VOLT_MEAS_NO = 2            # id=1はボタンに割り当てられている模様。1以外を使用する。
MEAS_INTERVAL_MODE_CNT = 3
COLOR_BLACK = 0x000000

volt_max_mode = 0
is_measure = False
meas_interval_ms = 1
meas_interval_mode = 0

M5Screen().clean_screen()
M5Screen().set_screen_bg_color(COLOR_BLACK)

# 各種ラベルの初期化
lbl_volt_max = M5Label("", x=45, y=220, color=0xffffff, font=FONT_MONT_18, parent=None)
lbl_volt_interval = M5Label("", x=145, y=220, color=0xffffff, font=FONT_MONT_18, parent=None)

# ボタン押下時の処理有効化
btnA.wasPressed(buttonA_wasPressed)
btnB.wasPressed(buttonB_wasPressed)
btnC.wasPressed(buttonC_wasPressed)

volt_meas = VoltMeasureBoard(GPIO_ADC_PIN, GPIO_RNG1_PIN, GPIO_RNG2_PIN, DISP_HIST_CNT)
volt_max_mode_cnt = volt_meas.get_volt_max_mode_cnt()
graph_volt = Graph("{:.2f}", "[V]")

# デバッグ用DAC出力（通常はコメントアウト）
# GPIO_DAC_PIN = 26
# dac = DAC(GPIO_DAC_PIN)
# dac.write(128)

# 電圧計測タイマ追加
tim_volt_meas = Timer(TIM_VOLT_MEAS_NO)
dac_val = 0

while True:
    lbl_volt_max.set_text(volt_meas.get_volt_max_str(volt_max_mode))    # 入力最大電圧表示
    volt_meas.set_div_resist(volt_max_mode)                             # 入力最大電圧設定に応じた回路設定変更
    graph_volt.set_max_min(volt_meas.get_volt_max(volt_max_mode), 0)    # 入力最大電圧設定に応じたグラフ最大値設定
    lbl_volt_interval.set_text("{}ms".format(meas_interval_ms))         # 電圧取得間隔表示
    if is_measure:                                                      # 計測時の処理
        graph_volt.clear_data()
        tim_volt_meas.init(period=meas_interval_ms, mode=tim_volt_meas.PERIODIC, callback=tim_volt_meas_callback)
        while True:                                                     # タイマ割り込みで電圧を取得する
            if graph_volt.is_plot_max():                                # 最大データ保存数まで取得したら終了
                break
            time.sleep(0.01)
        tim_volt_meas.deinit()                                          # タイマ停止
        graph_volt.plot_save_data(meas_interval_ms)                     # 取得したデータをグラフにプロットする
        is_measure = False
    time.sleep(0.01)
