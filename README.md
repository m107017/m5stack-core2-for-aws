# M5Stack Core2 for AWS

M5Stack Core2 for AWSを使用したプロジェクト

## 開発環境

- UIFlow(MicroPython) F/Wバージョン1.9.8
- Visual Studio Code(拡張機能「vscode-m5stack-mpy」使用)

## 各種プログラムの概要
### シリアルモニター
対象のデバイスからUART経由で送信されてくる文字列データを表示します。  
詳細は[こちら](SerialMonitor/doc/README.md)をご覧ください。  

### 電圧計
別途製作した外付け回路を経由して電圧を入力し、その電圧値を計測します。  
詳細は[こちら](VoltMeter/doc/README.md)をご覧ください。  

### 簡易オシロスコープ
別途製作した外付け回路を経由して電圧を入力し、その電圧値を一定時間計測します。計測結果をグラフ表示します。  
詳細は[こちら](SimpleOscilloscope/doc/README.md)をご覧ください。
