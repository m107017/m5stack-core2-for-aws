###############################################
# filename : serial_monitor.py
# function : シリアルモニタープログラム
###############################################

from m5stack import *
from machine import *
import time
# from char_display import CharDisplay      # クラス記述をファイル分割する場合はコメントアウトを外す

def buttonA_wasPressed():
    global scroll_up
    speaker.playTone(523, 0.2)
    scroll_up = True

def buttonB_wasPressed():
    global scroll_down
    speaker.playTone(523, 0.2)
    scroll_down = True

def buttonC_wasPressed():
    global read_stop
    speaker.playTone(523, 0.2)
    read_stop = not read_stop

UART_PC_ID = 2
UART_PC_BAUD = 115200

GPIO_UART_PC_TX = 14
GPIO_UART_PC_RX = 13

r_byte_list = bytearray()

uart_pc = UART(UART_PC_ID, baudrate=UART_PC_BAUD, tx=GPIO_UART_PC_TX, rx=GPIO_UART_PC_RX)
uart_pc.init(UART_PC_BAUD, bits=8, parity=None, stop=1)

char_disp = CharDisplay()
sleep_tim = 0
read_stop = False
scroll_up = False
scroll_down = False
btnA.wasPressed(buttonA_wasPressed)
btnB.wasPressed(buttonB_wasPressed)
btnC.wasPressed(buttonC_wasPressed)

while True:
    if scroll_up:
        char_disp.scroll(True)
        scroll_up = False
    if scroll_down:
        char_disp.scroll(False)
        scroll_down = False
    
    if uart_pc.any():
        sleep_tim = 0
        r_data = bytes(uart_pc.read(1))[0]
        if read_stop:
            pass
        else:
            if r_data == 0x0a:
                char_disp.append_line_byte(r_byte_list)
                r_byte_list = bytearray()
            if 0x20 <= r_data < 0x7f:
                r_byte_list.append(r_data)
            
            if len(r_byte_list) >= char_disp.get_max_disp_col_cnt():
                char_disp.append_line_byte(r_byte_list)
                r_byte_list = bytearray()
    else:
        time.sleep(0.1)
        sleep_tim += 1
        if sleep_tim == 5:
            if len(r_byte_list) != 0:
                char_disp.append_line_byte(r_byte_list)
                r_byte_list = bytearray()
            sleep_tim = 0
