@rem UTF-8
chcp 65001

@rem "電圧計プログラム作成"
@rem "本バッチファイルを実行すると、プログラム本体と必要自作クラスをマージし、1つのファイルに結合します。"
@rem "1つのファイルに結合することで、UIFlowやM5Stackでの実行や管理がしやすくなることを目論んでいます。"
@rem "あくまで実行や管理の容易性向上のため、結合後のファイル記述内容は実行できればよいものとしています。"

copy /b ..\MyClass\volt_measure.py + ..\MyClass\volt_measure_board.py + volt_meter.py volt_meter_exe.py
