###############################################
# filename : volt_meter.py
# function : 電圧計プログラム
###############################################

from m5stack import *
from m5stack_ui import *
from machine import *
import time
# from volt_measure_board import VoltMeasureBoard     # クラス記述をファイル分割する場合はコメントアウトを外す

def buttonA_wasPressed():
    global volt_max_mode, volt_max_mode_cnt
    speaker.playTone(523, 0.2)
    volt_max_mode += 1
    if volt_max_mode >= volt_max_mode_cnt:
        volt_max_mode = 0

def buttonB_wasPressed():
    global volt_save
    speaker.playTone(523, 0.2)
    volt_save = True

def buttonC_wasPressed():
    global hist_clear
    speaker.playTone(523, 0.2)
    hist_clear = True

def make_volt_str(val_volt):
    """電圧値表示文字列生成

    Args:
        val_volt (float): 電圧値[V]

    Returns:
        str: 電圧値表示文字列
    """
    str_volt = " " if val_volt < 10.0 else ""       # 10V未満の場合は、10の位の桁を半角スペースで埋める
    str_volt += "{:.2f}".format(val_volt)
    return str_volt

GPIO_ADC_PIN = 36
GPIO_RNG1_PIN = 32
GPIO_RNG2_PIN = 33
DISP_HIST_CNT = 6
COLOR_BLACK = 0x000000
COLOR_WHITE = 0xffffff

volt_max_mode = 0
volt_save = False
hist_clear = False

M5Screen().clean_screen()
M5Screen().set_screen_bg_color(COLOR_BLACK)

# 現在の電圧値表示ラベルの初期化
lbl_volt_val = M5Label(" 0.00", x=90, y=20, color=COLOR_WHITE, font=FONT_MONT_48, parent=None)
M5Label("[V]", x=215, y=40, color=COLOR_WHITE, font=FONT_MONT_26, parent=None)

# 保存した電圧値表示ラベルの初期化
lbl_hist_list = []
for i in range(DISP_HIST_CNT):
    if i % 3 == 0:      x = 10
    elif i % 3 == 1:    x = 110
    else:               x = 210
    y = 100 + 30 * int(i / 3)
    lbl_hist = M5Label(" 0.00 [V]", x=x, y=y, color=COLOR_WHITE, font=FONT_MONT_18, parent=None)
    lbl_hist_list.append(lbl_hist)

# 入力最大電圧ラベルの初期化
lbl_volt_max = M5Label("", x=45, y=220, color=0xffffff, font=FONT_MONT_18, parent=None)

# ボタン押下時の処理有効化
btnA.wasPressed(buttonA_wasPressed)
btnB.wasPressed(buttonB_wasPressed)
btnC.wasPressed(buttonC_wasPressed)

volt_meas = VoltMeasureBoard(GPIO_ADC_PIN, GPIO_RNG1_PIN, GPIO_RNG2_PIN, DISP_HIST_CNT)
volt_max_mode_cnt = volt_meas.get_volt_max_mode_cnt()

# デバッグ用DAC出力（通常はコメントアウト）
# GPIO_DAC_PIN = 26
# dac = DAC(GPIO_DAC_PIN)
# dac.write(128)

while True:
    lbl_volt_max.set_text(volt_meas.get_volt_max_str(volt_max_mode))    # 入力最大電圧表示
    volt_meas.set_div_resist(volt_max_mode)                             # 入力最大電圧設定に応じた回路設定変更
    val_volt = volt_meas.get_volt_val(volt_save)                        # 電圧計測
    if volt_save:                                                       # 電圧値を保存表示する場合の処理
        volt_save = False
        for lbl, val_volt in zip(lbl_hist_list, volt_meas.get_save_hist()):
            lbl.set_text(make_volt_str(val_volt) + " [V]")
    if hist_clear:                                                      # 電圧値保存クリア
        volt_meas.clear_save_hist()
        hist_clear = False
        for lbl in lbl_hist_list:
            lbl.set_text(" 0.00 [V]")
    lbl_volt_val.set_text(make_volt_str(val_volt))                      # 現在の電圧値表示
    time.sleep(0.1)
