###############################################
# filename : char_display.py
# function : 文字列表示クラス
###############################################

from m5stack import *

class CharDisplay:
    """文字列表示クラス

    M5Stackのディスプレイにて文字列表示を行う。
    フォントはデフォルトの設定を使用する。
    """

    def __init__(self, disp_line_cnt=18, font_color=lcd.WHITE, bg_color=lcd.BLACK):
        """初期化

        Args:
            font_color (_type_, optional): フォントの色
            bg_color (_type_, optional): 背景の色
        """
        self.font_color = font_color                # フォントの色
        self.bg_color = bg_color                    # 背景色
        
        self.max_disp_line_cnt = 18                 # デフォルトのフォントで表示できる行数
        self.max_disp_col_cnt = 35                  # デフォルトのフォントで表示できる1行あたりの文字数
        self.line_list = []                         # 行データを保存するリスト
        self.max_save_line_cnt = 100                # 保存する行データの行数

        # 表示する行数
        if disp_line_cnt > self.max_disp_line_cnt:
            self.disp_line_cnt = self.max_disp_line_cnt
        else:
            self.disp_line_cnt = disp_line_cnt

        self.scroll_offset = 0                      # スクロールしている行数

        self.clear()

    def __print(self, str, is_first_line=False):
        """文字表示

        Args:
            str (string): 表示文字列
            is_first_line (bool, optional): 先頭行に表示するか否か
        """
        if is_first_line:
            lcd.print(str, 0, 0, color=self.font_color)
        else:
            lcd.print(str, color=self.font_color)

    def get_max_disp_col_cnt(self):
        """1行に表示できる最大文字数取得

        Returns:
            int: 1行に表示できる最大文字数
        """
        return self.max_disp_col_cnt

    def clear(self):
        """表示クリア
        """
        lcd.clear(self.bg_color)

    def append_line_byte(self, byte_list, add_lf=True):
        """表示する行の追加（byte指定）

        Args:
            byte_list (bytearray): 表示する行情報（byte指定）
            add_lf (bool, optional): 引数byte_listの末尾に[LF](0x0a)を付加するか否か
        """
        if add_lf:
            byte_list.append(0x0a)
        self.line_list.append(byte_list.decode())
        self.update()

    def update(self):
        """画面更新
        """
        self.scroll_offset = 0
        if len(self.line_list) > self.disp_line_cnt:
            # データ保存行数が表示行数を超えたら、全画面更新
            if len(self.line_list) > self.max_save_line_cnt:
                # データ保存行数がフルになったら一番古いデータを削除
                self.line_list.pop(0)
            self.clear()
            for i in range(-self.disp_line_cnt, 0, 1):
                self.__print(self.line_list[i], i == -self.disp_line_cnt)
        else:
            # データ保存行数が表示行数を超えていない場合は、そのまま表示していく
            self.__print(self.line_list[-1])

    def scroll(self, is_up=True):
        """画面スクロール

        Args:
            is_up (bool, optional): True=UP / False=DOWN
        """
        if len(self.line_list) <= self.disp_line_cnt:
            # データ保存行数が表示行数を超えていない場合は、スクロール制御しない
            return
        if is_up:
            if self.disp_line_cnt + self.scroll_offset < len(self.line_list):
                self.scroll_offset += 1
        else:
            if self.scroll_offset > 0:
                self.scroll_offset -= 1
        self.clear()
        disp_start = -self.disp_line_cnt - self.scroll_offset
        disp_end_next = -self.scroll_offset
        for i in range(disp_start, disp_end_next, 1):
            self.__print(self.line_list[i], i == disp_start)
