###############################################
# filename : volt_measure_board.py
# function : 電圧計測クラス（分圧抵抗搭載基板使用）
###############################################

from machine import *
# from volt_measure import VoltMeasure      # クラス記述をファイル分割する場合はコメントアウトを外す

class VoltMeasureBoard(VoltMeasure):
    """電圧計測クラス（分圧抵抗搭載基板使用）
    """
    def __init__(self, gpio_adc, gpio_rng1, gpio_rng2, max_save_cnt, r1=0, r2=0):
        """初期化

        Args:
            gpio_adc (int): ADCとして使用するGPIO番号
            gpio_rng1 (int): 入力最大電圧設定に使用するGPIO番号（その1）
            gpio_rng2 (int): 入力最大電圧設定に使用するGPIO番号（その2）
            max_save_cnt (int): 最大保存データ数
            r1 (int, optional): 測定対象側の分圧抵抗値[Ω]（分圧無し時は0）
            r2 (int, optional): GND側の分圧抵抗値[Ω]（分圧無し時は0）
        """
        super().__init__(gpio_adc, max_save_cnt, r1, r2)
        self.gpio_rng1 = Pin(gpio_rng1, Pin.OUT)
        self.gpio_rng2 = Pin(gpio_rng2, Pin.OUT)
        self.volt_max_mode_cnt = 3

    def set_div_resist(self, volt_max_mode):
        """分圧抵抗値設定

        Args:
            volt_max_mode (int): 入力最大電圧設定
        """
        r1 = 100000
        r2 = 0
        if volt_max_mode == 0:
            r2 = 300000
            self.gpio_rng1.value(False)
            self.gpio_rng2.value(False)
        elif volt_max_mode == 1:
            r2 = 42860
            self.gpio_rng1.value(True)
            self.gpio_rng2.value(False)
        elif volt_max_mode == 2:
            r2 = 17650
            self.gpio_rng1.value(True)
            self.gpio_rng2.value(True)
        super().set_div_resist(r1, r2)

    def get_volt_max(self, volt_max_mode):
        """入力最大電圧値取得

        Args:
            volt_max_mode (int): 入力最大電圧設定

        Returns:
            int: 入力最大電圧値[V]
        """
        volt_max = 0
        if volt_max_mode == 0:
            volt_max = 4
        elif volt_max_mode == 1:
            volt_max = 10
        elif volt_max_mode == 2:
            volt_max = 20
        return volt_max

    def get_volt_max_mode_cnt(self):
        """入力最大電圧設定数取得

        Returns:
            int: 入力最大電圧設定
        """
        return self.volt_max_mode_cnt

    def get_volt_max_str(self, volt_max_mode):
        """入力最大電圧値文字列取得

        Args:
            volt_max_mode (int): 入力最大電圧設定

        Returns:
            str: 入力最大電圧値文字列
        """
        volt_max_val = self.get_volt_max(volt_max_mode)
        if volt_max_val == 0:
            return "ERROR"
        else:
            return "{}V".format(volt_max_val)
