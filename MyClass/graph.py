###############################################
# filename : graph.py
# function : グラフ表示クラス
###############################################

from m5stack import *

class Graph:
    """グラフ表示クラス

    固定枠のグラフを表示する。
    """
    def __init__(self, value_format, unit):
        """初期化

        Args:
            value_format (str): グラフ表示する値の数値表示フォーマット
            unit (str): グラフ表示する値の単位
        """
        self.x = 20
        self.y = 20
        self.w = 280
        self.h = 200
        self.frame_color = lcd.WHITE
        self.bg_color = lcd.BLACK
        self.plot_color = lcd.RED
        self.value_format = value_format
        self.unit = unit
        self.max = None
        self.min = None

        self.plot_data = []

        lcd.clear(self.bg_color)
        self.__draw_graph_frame()

    def __line_h(self, x1, x2, y):
        """水平線描画

        Args:
            x1 (int): 始点x座標
            x2 (int): 終点x座標
            y (int): y座標
        """
        lcd.line(x1, y, x2, y, self.frame_color)

    def __line_v(self, x, y1, y2):
        """垂直線描画

        Args:
            x (int): x座標
            y1 (int): 始点y座標
            y2 (int): 終点y座標
        """
        lcd.line(x, y1, x, y2, self.frame_color)

    def __draw_graph_frame(self, max=None, min=None, interval_ms=0):
        """グラフ枠表示

        Args:
            max (int, optional): グラフ表示最大値（None時は表示しない）
            min (int, optional): グラフ表示最小値（None時は表示しない）
            interval_ms (int, optional): 1プロットの時間間隔[ms]
        """
        lcd.rect(self.x, self.y, self.w, self.h, self.frame_color, self.bg_color)   # グラフの枠描画
        self.__line_v(self.x + self.w // 4, self.y, self.y + self.h)
        self.__line_v(self.x + self.w // 2, self.y, self.y + self.h)
        self.__line_v(self.x + self.w // 4 * 3, self.y, self.y + self.h)
        self.__line_h(self.x, self.x + self.w, self.y + self.h // 4)
        self.__line_h(self.x, self.x + self.w, self.y + self.h // 2)
        self.__line_h(self.x, self.x + self.w, self.y + self.h // 4 * 3)

        fw, fh = lcd.fontSize()                                                     # 幅は正しく取得できない
        if max != None and min != None:
            # 最大・最小値が指定されている場合は表示する
            disp_x = self.x + 2
            lcd.print(self.value_format.format(max) + self.unit, disp_x, self.y + 2, self.frame_color)
            lcd.print(self.value_format.format(min), disp_x, self.y + self.h - fh - 2, self.frame_color)
        if interval_ms != 0:
            lcd.print("{}".format(interval_ms * self.w // 2) + "[ms]", self.x + self.w // 2, self.y + self.h - fh - 2, self.frame_color)

    def __get_max_min(self):
        """最大値・最小値取得

        Returns:
            _type_: 最大値、最小値

        Note:
            グラフに描画する最大値と最小値を取得するために使用する。
            Noneを返す場合は、固定の最大・最小値ではなく、
            かつデータが存在しない場合。
        """
        val_max = None
        val_min = None
        if self.max != None and self.min != None:
            val_max = self.max
            val_min = self.min
        elif len(self.plot_data) != 0:
            val_max = max(self.plot_data)
            val_min = min(self.plot_data)
        return val_max, val_min

    def get_max_plot_count(self):
        """最大プロット数取得

        Returns:
            int: 最大プロット数（グラフ幅）
        """
        return self.w

    def is_plot_max(self):
        """プロットデータ数が最大数に達したか否かチェック

        Returns:
            bool: プロットデータ数が最大数に達したか否か
        """
        return len(self.plot_data) >= self.get_max_plot_count()

    def set_color(self, is_reload=True, frame_color=lcd.WHITE, bg_color=lcd.BLACK, plot_color=lcd.RED):
        """表示色変更

        Args:
            is_reload (bool, optional): 変更後再描画するか否か
            frame_color (_type_, optional): グラフの枠の色
            bg_color (_type_, optional): 背景色
            plot_color (_type_, optional): グラフのプロット色
        """
        self.frame_color = frame_color
        self.bg_color = bg_color
        self.plot_color = plot_color
        if is_reload:
            lcd.clear(self.bg_color)
            self.__draw_graph_frame()

    def set_max_min(self, max, min, is_reload=False):
        """グラフの最大最小値指定

        Args:
            max (int/float): グラフの最大値
            min (int/float): グラフの最小値
            is_reload (bool, optional): 変更後再描画するか否か
        
        Note:
            設定しない場合は、保存されているデータから最大・最小値を
            見つけて自動で表示する。
        """
        self.max = max
        self.min = min
        if is_reload:
            self.__draw_graph_frame()

    def set_new_data(self, value):
        """プロットデータ追加

        Args:
            value (int/float): _description_
        """
        if len(self.plot_data) < self.w:
            self.plot_data.append(value)

    def clear_data(self):
        """プロットデータクリア
        """
        self.plot_data.clear()

    def plot_save_data(self, interval=0):
        """保存したプロットデータ表示
        """
        max, min = self.__get_max_min()
        if max == None or min == None:
            return
        self.__draw_graph_frame(max, min, interval)
        plot_x = self.x + 1
        for val in self.plot_data:
            plot_y = self.y + self.h - 1 - int((val - min) / (max - min) * (self.h - 1))
            if plot_y <= self.y:            plot_y = self.y + 1
            elif plot_y >= self.y + self.h: plot_y = self.y + self.h - 1
            lcd.pixel(plot_x, plot_y, self.plot_color)
            plot_x += 1

