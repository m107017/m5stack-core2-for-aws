###############################################
# filename : volt_measure.py
# function : 電圧計測クラス
###############################################

from machine import *

class VoltMeasure:
    """電圧計測クラス

    A/D変換機能を使用して電圧計測を行う。
    """

    def __init__(self, gpio_adc, max_save_cnt, r1=0, r2=0):
        """初期化

        Args:
            gpio_adc (int): ADCとして使用するGPIO番号
            max_save_cnt (int): 最大保存データ数
            r1 (int, optional): 測定対象側の分圧抵抗値[Ω]（分圧無し時は0）
            r2 (int, optional): GND側の分圧抵抗値[Ω]（分圧無し時は0）
        """
        self.adc = ADC(gpio_adc)
        self.adc.width(ADC.WIDTH_12BIT)     # ADC 12ビット設定固定（デフォルト設定）
        self.adc.atten(ADC.ATTN_11DB)       # 減衰器11dB固定（約1/3.55倍）

        self.r1 = r1
        self.r2 = r2

        self.max_save_cnt = max_save_cnt    # 保存するデータ数
        self.hist = []                      # データ保存先

    def __save_volt_val(self, val_volt):
        """電圧値保存

        Args:
            val_volt (float): 電圧値[V]

        Note:
            最大保存データ数を超える場合は保存しない。
        """
        if len(self.hist) < self.max_save_cnt:
            self.hist.append(val_volt)

    def set_div_resist(self, r1, r2):
        """分圧抵抗値変更

        Args:
            r1 (int): 測定対象側の分圧抵抗値[Ω]（分圧無し時は0）
            r2 (int): GND側の分圧抵抗値[Ω]（分圧無し時は0）
        """
        self.r1 = r1
        self.r2 = r2

    def get_volt_val(self, is_save=False):
        """電圧値取得[V]

        Args:
            is_save (bool, optional): 電圧値を保存するか否か

        Returns:
            float: 電圧値[V]
        """
        val_adc = self.adc.read()
        val_volt = val_adc * 3.55 / 4096    # 減衰なしの場合は0～1V入力。11dBの減衰で3.55倍しているため、0～3.55V入力となる。
        if self.r1 != 0 and self.r2 != 0:   # 分圧抵抗値を考慮した電圧計算
            val_volt = (self.r1 + self.r2) * val_volt / self.r2
        if is_save:
            self.__save_volt_val(val_volt)
        return val_volt

    def get_save_hist(self):
        """保存データ取得

        Returns:
            list: 保存データ
        """
        return self.hist

    def clear_save_hist(self):
        """保存データクリア
        """
        self.hist.clear()
